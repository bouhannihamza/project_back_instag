import Mongoose from 'mongoose';
import User from './db/models/user';
import Like from './db/models/like';
import Comment from './db/models/comment';
const Schema = Mongoose.Schema;

var PostSchema = new Schema({
  id: String,
  description: String,
  img: {
    rel: String,
    href: String,
  },
  publisher:{
    name: String,
    ref: {type: Schema.Types.ObjectId, ref:"User"},
  },
  likes:[Like],
  comments:[Comment]
});

PostSchema.index({ name: 1});
let Post = Mongoose.model('Post', PostSchema);

export default Post;