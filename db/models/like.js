import Mongoose from 'mongoose';
import User from './db/models/user';
import Post from './db/models/post';
const Schema = Mongoose.Schema;

var LikeSchema = new Schema({
  publisher:User,
  post:Post,
  
});

LikeSchema.index({ name: 1});
let Like = Mongoose.model('Like', LikeSchema);

export default Like;