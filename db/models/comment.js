import Mongoose from 'mongoose';
import User from './db/models/user';
import Post from './db/models/post';


const Schema = Mongoose.Schema;

var CommentSchema = new Schema({
  value: String,
  publisher:User,
  post:Post,
  

});

CommentSchema.index({ name: 1});
let Comment = Mongoose.model('Comment', CommentSchema);

export default Comment;