import Mongoose from 'mongoose';
import Post from './db/models/post';
import Like from './db/models/like';
import Comment from './db/models/comment';

const Schema = Mongoose.Schema;

var UserSchema = new Schema({
  username: String,
  password : String,
  description: String,
  img: {
    rel: String,
    href: String,
  },
  followers:[User],
  following:[User],
  posts:[Post],
  likes:[Like],
  comments:[Comment]
    
});

UserSchema.index({ name: 1});
let User = Mongoose.model('User', UserSchema);

export default User;