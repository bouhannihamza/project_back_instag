import user from "./models"

export async function getbyPage(page, per_page){
    var start = (parseInt(page)-1)* parseInt(per_page);
    let result = await user.find({})
    .populate({
        path: "publisher.ref",
        model: "User"
    })
    .skip(start)
    .limit(parseInt(per_page));
    return result;
}


export async function createUser(user){
    if (user){
        if(!user._id){
            console.log("[user]" - Creation);
            return user.create({ ...user});
        }
    }
}